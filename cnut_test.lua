-- tests for cnut.lua

local tests = 0
local success = 0

function test(title, f)
    modulus = nil
    modpoly = nil
    print("TEST", title)
    tests = tests + 1
    local status, err = pcall(f)
    if status then
        success = success + 1
    else
        print("FAILED", err)
    end
end

function END()
    print(success .. " out of " .. tests .. " tests succeeded.")
end

require 'cnut'

test('identifying polynomials', function()
    assert(POLY.is(poly {1}), 'poly is a poly')
    assert(not POLY.is(1), 'int is not a poly')
end)

test('polynomial add mul 0 and 1, no modulus', function()
    a = poly { 0 }
    b = poly { 1 }
    c = poly {1, 2, 3}
    assert(a * b == a, "0 * 1 should be 0")
    assert(a * c == a, "mul with 0 should be 0")
    assert(b * b == b, "1 * 1 = 1")
    assert(b * c == c, "1 * c = c")
    assert(a + a == a, "0 + 0 = 0")
    assert(a + c == c, "0 + c = 0")
    assert(c + a == c, "c + 0 = 0")
    assert(not(b + c == c), "1 + c != c")
end)

test('polynomial add, no modulus', function()
    a = poly {0, 2, 0, 1}
    b = poly {3, 1, 2}
    assert(a + b == poly {3, 3, 2, 1}, "addition")
end)

test('polynomial add mod 5', function()
    modulus = 5
    a = poly {3, 4, 3, 3}
    b = poly {4, 4}
    assert(a + b == poly {2, 3, 3, 3}, "a + b")
    assert(b + a == poly {2, 3, 3, 3}, "b + a")
end)

test('polynomial mul mod 5', function()
    modulus = 5
    a = poly {3, 2, 1}
    b = poly {1, 4, 2}
    assert(a * b == poly {3, 4, 0, 3, 2}, "mul")
    assert(a * b == b * a, "mul commute")
end)

test('print', function()
    assert(tostring(poly {1, 0, 2}) == "(1 + 2 X^2)")
    assert(tostring(poly {1}) == "(1)")
    assert(tostring(poly {0, 1}) == "(1 X)")
    assert(tostring(poly {0, 0, 1}) == "(1 X^2)")
    assert(tostring(poly {0}) == "(0)")
end)

test('GF(3^3)', function()
    modulus = 3
    modpoly = poly {1, 2, 0, 1}
    a = poly {0, 1}
    p = a * a
    assert(p == poly {0, 0, 1}, 'X^2')
    p = p * a
    assert(p == poly {2, 1}, 'X^3 = X + 2')
    assert(#p == 2, "degree")
    p = p * a
    assert(p == poly {0, 2, 1}, 'X^4 = X^2 + 2X')
    p = p * a
    assert(p == poly {2, 1, 2}, 'X^5 = 2X^2 + X + 2')
    p = p * a
    assert(p == poly {1, 1, 1}, 'X^6 = X^2 + X + 1')
    p = p * a
    assert(p == poly {2, 2, 1}, 'X^7 = X^2 + 2X + 2')
    p = p * a
    assert(p == poly {2, 0, 2}, 'X^8 = 2X^2 + 2')
    p = p * a
    assert(p == poly {1, 1}, 'X^9 = X + 1')
    p = p * a
    assert(p == poly {0, 1, 1}, 'X^10 = X^2 + X')
end)

test('poly evaluation', function()
    assert(POLY.eval(poly {1, 3, 2}, 2) == 15, '(1+3X+2X^2)|(X=2) = 15')
    modulus = 7
    assert(POLY.eval(poly {1, 3, 2}, 2) == 1, '(1+3X+2X^2)|(X=2) = 1 mod 7')
end)

END()

