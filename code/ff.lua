-- finite field arithmetic

F = require "F"

element = function(list)
    local r = {}
    local n = 0
    for k, v in ipairs(list) do
        n = n + 1
        table.insert(r, v)
    end
    r.n = n
    r.__type = 'element'
    setmetatable(r, {
        __tostring = function(t)
            local a = {}
            local iszero = true
            for k = 1, t.n do
                if r[k] ~= 0 then
                    iszero = false
                    local v = ''
                    if k == 1 then
                        v = ''
                    elseif k == 2 then
                        v = '.X'
                    else
                        v = '.X^' .. tostring(k-1)
                    end
                    table.insert(a, tostring(r[k]) .. tostring(v))
                end
            end
            if iszero then table.insert(a, '0') end
            return table.concat(a, ' + ')
        end
    })
    return r
end

e_tostring = function(t, var)
    if var == nil then var = 'X' end
    local a = {}
    local iszero = true
    for k = 1, t.n do
        if t[k] ~= 0 then
            iszero = false
            local v = ''
            if k == 1 then
                v = ''
            elseif k == 2 then
                v = '.' .. var
            else
                v = '.' .. var .. '^' .. tostring(k-1)
            end
            table.insert(a, tostring(t[k]) .. tostring(v))
        end
    end
    if iszero then table.insert(a, '0') end
    return table.concat(a, ' + ')
end

add = function(a, b, p)
    assert(a.__type == 'element')
    assert(b.__type == 'element')
    assert(a.n == b.n)
    local r = {}
    for i = 1, a.n do
        table.insert(r, (a[i] + b[i]) % p)
    end
    return element(r)
end

neg = function(a, p)
    return element(F.map(function(x) return (p-x) end, a))
end

-- transformation. A matrix of values representing an isomorphism between
-- finite fields. Coefficients are in transposed form, that is the j-th row is
-- the value of the transformation at the j-th base element. Within a row, the
-- lowest-order coefficient comes first.
trans = function(ll)
    local r = {}
        local n = #ll
        for _, v in ipairs(ll) do
            assert(#v == n)
            table.insert(r, v)
        end
        r.n = n
        r.__type = 'trans'
    return r
end

apply = function(t, x, p)
    assert(t.__type == 'trans')
    assert(x.__type == 'element')
    assert(t.n == x.n)
    local r = {}
    for i = 1, t.n do
        for j = 1, t.n do
            if r[i] == nil then r[i] = 0 end
            --[[
            print(i .. '/' .. j .. ' r=' .. tostring(r[i]) ..
                ' t=' .. tostring(t[j][i]) ..
                ' x=' .. tostring(x[j]) ..
                ' =>' .. tostring((r[i] + t[j][i] * x[j]) % p))
            ]]
            r[i] = (r[i] + t[j][i] * x[j]) % p
        end
    end
    return element(r)
end

eq = function(a, b)
    assert(a.__type == 'element')
    assert(b.__type == 'element')
    assert(a.n == b.n)
    for i = 1, a.n do
        if a[i] ~= b[i] then return false end
    end
    return true
end

-- variable(s) over GF(2).
variable = function(name)
    local r = {
        __type = 'var',
        names = {}
    }
    r.names[name] = true
    setmetatable(r, {
        __eq = function(x, y)
            if not (type(x) == 'table' and x.__type == 'var') then return false end
            if not (type(y) == 'table' and y.__type == 'var') then return false end
            for k, v in pairs (x.names) do
                if x.names[k] == true and not y.names[k] == true then
                    return false
                end
            end
            for k, v in pairs (y.names) do
                if y.names[k] == true and not x.names[k] == true then
                    return false
                end
            end
            return true
        end,
        __mod = function(x, p) return x end,
        __add = function(x, y)
            if type(y) == 'number' and y == 0 then
                return x
            elseif type(x) == 'number' and x == 0 then
                return y
            end

            local sum = variable('*')
            sum.names['*'] = false
            for k, v in pairs(x.names) do
                if v then sum.names[k] = true end
            end
            for k, v in pairs(y.names) do
                if v and x.names[k] then
                    sum.names[k] = false
                elseif v then
                    sum.names[k] = true
                end
            end
            local iszero = true
            for _, v in pairs(sum.names) do
                if v then iszero = false end
            end
            if iszero then
                return 0
            else
                return sum
            end
        end,
        __tostring = function(t)
            assert(t.__type == 'var')
            local vars = {}
            for k, v in pairs(t.names) do
                if v then table.insert(vars, k) end
            end
            return '(' .. table.concat(vars, "+") .. ')'
        end,
        __mul = function(x, y)
            local a, b
            if type(x) == 'table' and x.__type == 'var'
               and type(y) == 'number' then
               a = x
               b = y
            elseif type(y) == 'table' and y.__type == 'var'
                and type(x) == 'number' then
                a = y
                b = x
            else
                error("Type error.")
            end
            if b == 1 then
                return a
            elseif b == 0 then
                return 0
            else
                error()
            end
        end
    })
    return r
end

-- mod p(X) multiplication modulo 2

mul = function(a, b, m, p)
    assert(a.__type == 'element')
    assert(b.__type == 'element')
    assert(m.__type == 'element')
    assert(a.n == b.n and b.n == m.n)
    assert(p == 2)

    local zero = F.map(function(_) return 0 end, a)
    local x = element(a)
    local y = element(b)
    local r = element(zero)
    local n = a.n

    local shu = function(t)
        table.insert(t, 1, 0)
        table.remove(t)
    end
    local shd = function(t)
        table.insert(t, 0)
        table.remove(t, 1)
    end

    while not eq(x, element(zero)) do
        if x[1] ~= 0 then r = add(r, y, 2) end
        shu(y)
        shd(x)
        if y[n] ~= 0 then y = add(y, m, 2)  end
    end

    return r
end

-- General GF(p^n) multiplication function.
-- a: first factor.
-- b: second factor.
-- m: modulus polynomial.
-- p: characteristic (order of base field).
gmul = function(a, b, m, p)
    assert(a.__type == 'element')
    assert(b.__type == 'element')
    assert(m.__type == 'element')
    assert(a.n == b.n and b.n == m.n)
    local zero = F.map(function(_) return 0 end, a)
    local x = element(a)
    local y = element(b)
    local r = element(zero)
    local n = a.n
    local w = element(F.map(function(x) return (p-x) end, m))

    -- print("X^" .. (#m+1) .. "=" .. tostring(w))
    local shu = function(t) -- modifies t in place
        local c = t[n]
        table.insert(t, 1, 0)
        table.remove(t)
        if (c ~= 0) then -- shifted out a carry, so reduce mod m
            local f = F.map(function(x) return (c*(p-x)) % p end, m)
            for i = 1, #t do
                t[i] = (t[i] + f[i]) % p
            end
        end
    end
    local shd = function(t) -- modifies t in place
        table.insert(t, 0)
        table.remove(t, 1)
    end
    while not eq(x, element(zero)) do
        -- print("A=" .. tostring(x) .. "  B=" .. tostring(y))
        if x[1] ~= 0 then
            for i = 1, #r do
                r[i] = (r[i] + y[i] * x[1]) % p
            end
        end
        shu(y)
        shd(x)
    end
    return r
end

function is_iso(f, p, q, elements)
    local iso = true
    F.each(function(a)
        F.each(function(b)
            local x = mul(a, b, p, 2)
            local s = add(a, b, 2)
            local fx = apply(f, x, 2)
            local fs = apply(f, s, 2)
            local fa = apply(f, a, 2)
            local fb = apply(f, b, 2)
            if not eq(mul(fa, fb, q, 2), fx) then
                iso = false
                print("Product mismatch.\n" ..
                    "  a=" .. tostring(a) .. " b=" .. tostring(b) ..
                    "  ab=" .. tostring(x) .. "\n" ..
                    "  fa=" .. tostring(fa) .. " fb=" .. tostring(fb) ..
                    "  fab=" .. tostring(fx)
                )
            end
            if not eq(add(fa, fb, 2), fs) then
                iso = false
                print("Sum mismatch.\n" ..
                    "  a=" .. tostring(a) .. " b=" .. tostring(b) ..
                    "  a+b=" .. tostring(s) .. "\n" ..
                    "  fa=" .. tostring(fa) .. " fb=" .. tostring(fb) ..
                    "  f.a+b=" .. tostring(fs)
                )
            end
        end, elements)
    end, elements)
    return iso
end

-- Generalised isomorphism check.
-- f: transformation
-- src: source modulus polynomial.
-- dest: destination modulus polynomial.
-- p: characteristic of base field.
-- n: extension degree.
-- RETURNS: boolean, description of first failure if false.
function g_is_iso(f, src, dest, p, n)
    for a in enum_field(p, n) do
        for b in enum_field(p, n) do
            local s_sum = add(a, b, p)
            local s_prod = gmul(a, b, src, p)
            local d_sum = apply(f, s_sum, p)
            local d_prod = apply(f, s_prod, p)

            local aa = apply(f, a, p)
            local bb = apply(f, b, p)
            local sum2 = add(aa, bb, p)
            local p2 = gmul(aa, bb, dest, p)
            if not eq(d_sum, sum2) then
                return false, 'sum of ' .. tostring(a) .. ' and ' .. tostring(b)
            end
            if not eq(d_prod, p2) then
                return false, 'product of ' .. tostring(a) .. ' and ' .. tostring(b)
            end
        end
    end
    return true, nil
end

-- compute (a(X)):(X-t) in GF(p^n) represented by q(X).
-- a must be a poly with implicit leading coefficient 1, high-order
-- coefficients first. a is modified in place.
polydiv1 = function(a, t, p, q)
    assert(type(a) == "table")
    assert(t.__type == "element")
    assert(q.__type == "element")
    assert(t.n == q.n)
    local r = {}

    local z = {}
    for i = 1, #t do table.insert(z, 0) end
    local zero = element(z)

    for i = 1, #a-1 do
        table.insert(r, add(zero, a[i], p))
        a[i] = zero
        a[i+1] = add(a[i+1], gmul(t, r[i], q, p), p)
    end
    return r, a[#a]
end

function search3(p)
    local cp = function(a0, a1, a2, a3)
        local irred = true
        for x = 0, p-1 do
            local r = (a0 + a1 * x + a2 * x * x + a3 * x * x * x) % p
            if irred == true and r == 0 then
                print(a0 .. " + " .. a1 .. " X + " .. a2 .. " X^2 + " .. a3 ..
                    " X^3" ..
                    " has linear factor (X-" .. x ..") mod " .. p)
                irred = false
            end
        end
        if irred == true then
            print(a0 .. " + " .. a1 .. " X + " .. a2 .. " X^2 + " .. a3 ..
                " X^3" ..
                " is irreducible mod " .. p)
        end
    end
    for a0 = 0, p-1 do
        for a1 = 0, p-1 do
            for a2 = 0, p-1 do
                for a3 = 0, p-1 do
                    cp(a0, a1, a2, a3)
                end
            end
        end
    end
end

function isos()
    elements = F.map(element, {
        {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
        {0, 0, 1}, {1, 0, 1}, {0, 1, 1}, {1, 1, 1}
    })

    phi1 = trans({{1, 0, 0}, {0, 0, 1}, {0, 1, 1}})
    phi1y = trans({{1, 0, 0}, {0, 0, 1}, {1, 1, 1}})
    phi2y = trans({{1, 0, 0}, {1, 1, 1}, {0, 1, 0}})

    f1 = trans({{1, 0, 0}, {1, 0, 1}, {0, 1, 1}})
    f2 = trans({{1, 0, 0}, {1, 1, 0}, {1, 0, 1}})
    f3 = trans({{1, 0, 0}, {0, 1, 1}, {1, 1, 0}})

    x = element({0, 1, 0})
    xx = element({0, 0, 1})

    -- check that f1 is an iso
    p = element({1, 1, 0})
    q = element({1, 0, 1})

    print("f1 iso:" .. tostring(is_iso(f1, p, q, elements)))
    print("f2 iso:" .. tostring(is_iso(f2, p, q, elements)))
    print("f3 iso:" .. tostring(is_iso(f3, p, q, elements)))
end

function find_factors3()
    p = {1, 0, 2, 1} -- X^3 + 2X + 1
    q = {1, 2, 1, 1} -- Y^3 + 2Y^2 + Y + 1

    local pt = F.map(function(x) return element({x, 0, 0}) end, p)
    local qt = element({1, 1, 2})

    for a = 0, 2 do for b = 0, 2 do for c = 0, 2 do
        local x = element({a, b, c})
        local c = F.map(function(x) return x end, pt)
        local d, r = polydiv1(c, x, 3, qt)
        if r == 0 then
            print("zero at " .. e_tostring(x, 'Y'))
        else
            -- check it really factors
            local c = F.map(function(x) return x end, pt)
            c[#c] = add(c[#c], neg(r, 3), 3)
            local d2, r2 = polydiv1(c, x, 3, qt)
            assert(eq(r2, element({0, 0, 0})))
            -- end check
            print(e_tostring(x, 'Y') .. " has remainder " .. e_tostring(r, 'Y'))
        end

    end end end
end

-- iterator that enumerates all elements of GF(p^n).
function enum_field(p, n)
    local c = F.ntimes(0, n)
    c[1] = -1
    local done = false
    return function()
        if done == true then return nil end
        local i = 1
        while i ~= 0 do
            c[i] = c[i] + 1
            if c[i] == p then
                c[i] = 0
                i = i + 1
                if i > n then
                    done = true
                    return nil
                end
            else
                i = 0
            end
        end
        return element(c)
    end
end

-- compute a map f from f(X).
-- fx: value of f at X.
-- m: modulus polynomial.
-- p: base field order.
function extend_map(fx, m, p)
    local l = {}
    table.insert(l, F.ntimes(0, #fx))
    l[1][1] = 1
    table.insert(l, F.map(F.id, fx))
    local y = F.map(F.id, fx)
    local z = F.map(F.id, fx)
    for i = 3, #fx do
        z = gmul(element(y), element(z), m, p)
        table.insert(l, F.map(F.id, z))
        --[[
        table.insert(y, 1, 0)
        local c = table.remove(y)
        if c ~= 0 then
            -- reduce mod m
            y = F.map2(function(a, b) return (a - c * b) % p end, y, m)
        end
        table.insert(l, F.map(F.id, y))
        ]]
    end
    return trans(l)
end

-- pretty-print a transformation.
function print_trans(t)
    assert(t.__type == "trans")
    for i = 1, #t do
        print("X^" .. (i-1) .. " |--> " .. e_tostring(element(t[i])))
    end
end

-- main --

--[[
F.each(function(e)
    local x = apply(phi1, e, 2)
    local y = apply(f1, x, 2)
    local a = apply(f1, e, 2)
    local b = apply(phi1y, a, 2)
    if not eq(y, b) then
        print("Mismatch at " .. tostring(e))
    else
        print("ok at " .. tostring(e))
    end
    print("  x=" .. tostring(x) .. ", y=" .. tostring(y))
    print("  a=" .. tostring(a) .. ", b=" .. tostring(b))
end, elements)
]]

