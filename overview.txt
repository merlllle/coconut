[ ] Groups
    [ ] Motivation
    [ ] Definition of a group
    [ ] Associativity and commutativity
    [ ] The groups (Z; +) and (Zn; +)
    [ ] Order of a group
    [ ] The �modulo n� operation
    [ ] Division with remainder
    [ ] Equivalence relations and classes
    [ ] Representative elements and Zn
    [ ] Computing in Zn
    [ ] Formal construction of Zn
[ ] Subgroups
    [ ] Opening example
    [ ] Definition of a subgroup
    [ ] Generators
    [ ] Order of elements and Lagrange�s theorem
    [ ] Permutation groups
    [ ] Subgroups of permutation groups
    [ ] Cycles
    [ ]  Cosets and a proof of Lagrange�s theorem.
[ ] Rings and multiplication
    [ ] Rings
    [ ] Units and zero divisors
    [ ] The group Z n
    [ ] Euler�s totient function
    [ ] Exponent arithmetic
    [ ]  Cancellation and integral domains
[ ] Fields and Euclid�s algorithm
    [ ] Fields
    [ ] Finite rings and fields
    [ ] Characteristics
    [ ] Euclid�s algorithm
[ ] Homomorphisms
    [ ] Group homomorphisms
    [ ] Isomorphisms
    [ ] Group isomorphisms
    [ ] Products of groups
    [ ] Classification of finite Abelian groups
    [ ] The group of automorphisms
    [ ] Ring homomorphisms
    [ ] Field homomorphisms
    [ ]  More on finite Abelian groups
[ ] Finite Fields
    [ ] Finite commutative rings
    [ ] Classification of finite fields
    [ ] Prime fields
    [ ] Irreducible polynomials
    [ ] Finding irreducible polynomials
    [ ] Example: GF (72)
    [ ] Automorphisms of GF (72)
    [ ] Isomorphisms in GF (72)
    [ ] Example: GF (28)
    [ ] Implementation of GF (28) multiplication
    [ ] Automorphisms of finite fields
    [ ] Isomorphisms of GF (28)
    [ ] Factoring polynomials in SAGE
[ ] Vector spaces
    [ ] Definitions
    [ ] Linear independence and bases
    [ ] Linear maps
    [ ] Polynomial spaces as vector spaces
    [ ] Automorphisms revisited
[ ] Polynomials
    [ ] Definition of polynomials
    [ ] Degree
    [ ] Polynomial arithmetic
    [ ] Fields modulo polynomials
    [ ]  Representatives modulo p
    [ ]  Euclidean domains

CODING THEORY

[ ] Introduction to Coding Theory
    [ ] Introduction
    [ ] Error-Correcting Codes
        [ ] Notation
        [ ] Hamming distance and minimum distance
    [ ] Sage
[ ] Linear codes
    [ ] Vector spaces over finite fields
    [ ] Linear codes
        [ ] Generator and parity-check matrix
        [ ] Singleton bound and MDS codes
        [ ] Reed-Solomon codes
        [ ] Optimality problem
        [ ] Hamming codes
        [ ] Sage codes
[ ] Decoding
    [ ] Decoding problem
    [ ] decoding linear codes
        [ ] cosets
        [ ] syndromes
    [ ] Sage

INFORMATION THEORY

[ ] Information theory I
    [ ] Introduction
    [ ] Discrete probability
    [ ] Decoding and likelihood principle
    [ ] random variables
[ ] Information theory II
    [ ] entropy
    [ ] conditional entropy
    [ ] <> Channel capacity
[ ] Information theory III
    [ ] source codes
    [ ] compression
    [ ] Huffman encoding
[ ] Perfect secrecy
    [ ] encryption
    [ ] one-time pad


