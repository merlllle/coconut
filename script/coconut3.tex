\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{3}{Rings and Multiplication}

\begin{document}

\titlestuff

\emph{In this lecture:} rings --- units and zero divisors ---
the group $\mathbb Z^\times_n$ --- Euler's totient
function --- exponent arithmetic

\emph{Learning outcomes.}
After this lecture and revision, you should be able to:
\begin{itemize}
\item Find the order of the multiplicative group modulo any $n$.
\item Multiply and invert in $\mathbb Z^\times_n$.
\item Tell if a structure is a ring.
\item Classify ring elements as units, zero divisors or neither.
\item Solve equations of the form $ax = b \pmod{n}$.
\item Perform exponent arithmetic.
\end{itemize}
\fi

\section{Rings and multiplication}

The last two lectures, we have looked at groups which are a generalisation of
addition. Today we look at multiplication.

\subsection{Rings}

We began this course by saying that a group is a bit like adding numbers. This
is not quite true --- a group is a bit like adding and subtracting numbers,
since there must always be inverses. If we introduce multiplication as a group
operation, this gives us division too but sometimes that is more than we need.
In $\mathbb Z$ for example, you can multiply but you cannot always divide.
A ring is a structure a bit like numbers with addition, subtraction and
multiplication but you cannot necessarily divide (even when there is no $0$
involved).

\begin{definition}[ring]
A structure $\mathcal R = (R, +, \times)$ where $R$ is a set and $+, \times$ are
two operations $R \times R \to R$ is a ring if the following hold.

\begin{description}
\item[additive group] The structure $(R, +)$ is an Abelian group. We call its
neutral element the zero of $R$ and write it with the symbol $0$ or sometimes
$0_R$.
\item[multiplication] The structure $(R, \times)$ is associative
and has a neutral element, which we call the one of the ring and write as $1$
or $1_R$.
\item[distributive law]
For any elements $a, b, c$ in $R$ we have $(a + b) \times c = a \times c + b
\times c$ and $c \times (a + b) = c \times a + c \times b$.
\end{description}
\end{definition}

The description of a ring requires the additive group to be commutative but not
ncessarily the multiplication. We call a ring commutative if the multiplication
is commutative too.

The standard example of a ring is $(\mathbb Z, +, \times)$. For any positive
integer $n$, the space $(\mathbb Z_n, +_n, \times_n)$ is a ring too.  If we take
$n$-tuples of integers with componentwise addition and multiplication, this
gives us yet another ring.

We state some basic facts about rings:

\begin{proposition}
\rule{0pt}{0pt}
\begin{itemize}
\item There can only be one neutral element for multiplication in any ring.
\item There is a ring with one element, which is the neutral element for both
addition and multiplication ($0 = 1$!). As soon as a ring has at least two
elements, the neutral elements $0$ for addition and $1$ for multiplication must
be different however.
\item Multiplying any element $x$ in a ring with $0$, the neutral element of
addition, gives $0$ (from both the left and the right, if the ring is not
commutative).
\end{itemize}
\end{proposition}

\subsection{Units and zero divisors}

In a ring, things can happen that we might not expect from the usual integers.
For example, the structure $(\mathbb Z_8, +_8, \times_8)$ is a ring in which $2
\times_8 4 = 0$, so a product of two nonzero elements can be zero even if
neither of the factors are. If we take the ring of pairs of integers with
component-wise addition and multiplication, $(0, 1) \times (1, 0) = (0, 0)$:
another case where the product of two nonzero elements becomes zero.

We can classify all elements in a ring into the following classes.

\begin{definition}[unit, zero divisor]
In a ring $(R, +, \times)$:

\begin{itemize}
\item The neutral element of addition is called the \hi{zero} of the ring and
written with the symbol $0$.
\item An element $a$ in the ring which has a multiplicative inverse $b$,
i.e. $a \times b = 1$ and $b \times a = 1$, is called a \hi{unit} of the ring.
The neutral element of multiplication is always a unit.
\item An element $a \neq 0$ for which there is some other $b \neq 0$ such that
$a \times b = 0$ or $b \times a = 0$ is called a \hi{zero divisor}.
\item An element can also be \hi{neither} of the above.
\end{itemize}
\end{definition}

Most of the time, the classification zero/unit/zero divisor/none of the
previous is unique, i.e. each element of the ring is in exactly one class. The
only exception is that in the ring with one element, the element is both the
zero and a unit; as soon as $1 \neq 0$ a unit can neither be zero nor a zero
divisor.

\subsection{The group $\mathbb Z^\times_n$}

Let's try and build a structure where multiplication is a group operation.
Start by looking at multiplication in $\mathbb Z$. Multiplication is
associative and $1$ is the neutral element. However, $0$ causes problems if we
try and find inverses: $0 \times a = 0$ for all $a$ so there cannot be an $a$
with $0 \times a = 1$.  So let's get rid of $0$ and look at $\mathbb Z^\times =
\mathbb Z \setminus \{0\}$. We still don't have inverses: there is no integer
$z$ satisfying $3 \times z = 1$ for example.  The usual way to carry on at this
point is to introduce the fractions (without zero) $\mathbb Q^\times$ which
form a group under multiplication.

Now let's look at the group $(\mathbb Z_{2^8}, +) = (\mathbb Z_{256}, +)$; this
group is sometimes called \texttt{byte}, \texttt{uint8} or \texttt{unsigned
char} in programming. What happens if we introduce multiplication on this group
with the rule that if you exceed 255, you subtract 256 until you are back in
range (equivalently, you ignore all ``higher bits'' of a number)?

Interestingly, we can solve the equation $3 \times_{256} z = 1$ where
$\times_{256}$ is multiplication modulo $256$. The solution is $z = 171$ since
$3 \times 171 = 513 = 2 \times 256 + 1$ (division with remainder) so $3
\times_{256} 171 = 1$. So in some cases we can find inverses without resorting
to fractions.

We quickly hit another problem though: $16 \times 16 = 256$, so $16 \times_{256}
16 = 0$ so $16$ cannot have an inverse, fractions or no: if $16 \times_{256} a =
1$ had any solution for $a$, we could multiply both sides of the equation to
get $0 = 16$ which is nonsense, even modulo $256$.

Let's throw out all elements that cause problems from $\mathbb Z_n$. $0$ is
right out. Anything that gives $0$ when multiplied with another number from
$\mathbb Z_n$ except $0$ is out too (that is, all zero divisors). This leaves
us with a group:

\begin{proposition}[the group $\mathbb Z^\times_n$]
For a positive integer $n$, the set $\mathbb Z^\times_n$ is the subset of
$\mathbb Z_n$ containing all elements $a$ for which $a \times_n b \neq 0$ for
all other elements $b \neq 0 \in \mathbb Z_n$. Together with multiplication
modulo $n$, this forms a group $(\mathbb Z^\times_n, \times_n)$.
\end{proposition}

Let's check that this really is a group. Associativity follows from that of
$\times$ on $\mathbb Z$, the same way we did it for $+_n$ in the first lecture.
The neutral element is $1$ since $1 \times_n b = b$ for any $b \neq 0 \in
\mathbb Z_n$ and if $b \neq 0$ then $1 \times_n b = b$ is not $0$ either, so we
can't lose the element $1$ in the definition of $\mathbb Z^\times_n$.
To find inverses we have to invoke a bit more number theory.

\begin{exercise}
$(\star)$ \emph{Arithmetic modulo primes.}
In this exercise, we choose the prime $p = 1009$ as our modulus and look at the
ring $\mathcal R = (\mathbb Z_{1009}, +, \times)$.

\begin{enumerate}
\item Compute $824 + 3 \times 632$ in $\mathcal R$.
\item What is the order of $7$ in $(\mathbb Z_{1009}, +)$?
\end{enumerate}
\end{exercise}

\begin{exercise}
$(\star)$ \emph{Addition and multiplication tables.}
\begin{itemize}
\item Compute the addition and multiplication tables for $(\mathbb Z_5, +, \times)$.
\item Do the same for $(\mathbb Z_4, +, \times)$.
\item Write the group table for the group $(\mathbb Z^\times_4, \times)$.
\end{itemize}
\end{exercise}

\begin{exercise}
$(\star\star)$ \emph{Arithmetic modulo $n$.}
Consider $n = 16$. Find all solutions modulo $n$ of the equations
\begin{enumerate}
\item $5x = 1$
\item $6y = 1$
\item $8z = 0$
\end{enumerate}
\end{exercise}

\subsection{Euler's totient function}

How many elements are left over in $\mathbb Z^\times_n$? We give this quantity
a name.

\begin{definition}[Euler totient function]
Euler's totient function $\phi$ is the function $\phi(n) := |\mathbb
Z^\times_n|$ for $n > 0\in \mathbb N$ that maps a positive integer $n$ to the
number of elements in $\mathbb Z^\times_n$.
\end{definition}

To compute this function, we can ask the related question, which elements have
we got rid of? Let's take an element $a \neq 0$ that was in $\mathbb Z_n$ but
not in $\mathbb Z^\times_n$, which means there is some $b \neq 0$ in $\mathbb
Z_n$ such that $a \times_n b = 0$.  The definition of $a \times_n b$ is the
remainder when dividing $a \times b$ by $n$, so we must have $a \times b = c
\times n + 0$, i.e. the remainder is $0$ so $a \times b$ is a multiple of $n$.
Since we assumed $a \neq 0$ and $b \neq 0$ then $c$ cannot be $0$ either: the
product of two nonzero elements in $\mathbb Z$ is never zero. Further if $b$ is
not a multiple of $n$ on its own, which it cannot be because $b \in \mathbb
Z_n$ so $y < n$, then $a$ and $n$ must have a factor in common (that is higher
than $1$).  Conversely, if $a$ and $n$ have the common factor $k$ then we can
write $a = \alpha k, n = \beta k$ for some nonzero integers $\alpha, \beta$ and $\beta \times a = \alpha
\times \beta \times k = \alpha \times n$ which is a multiple of $n$ so $a \times_n \beta = 0$.
So a number in $\mathbb Z_n$ is excluded from $\mathbb Z^\times_n$ if and only
if it has a common factor (other than $1$) with $n$.

This tells us $\phi(p)$ whenever $p$ is a prime. Since primes are exactly those
numbers that have no factors except $1$ and themselves, the only element of
$\mathbb Z_p$ that is excluded from $\mathbb Z^\times_p$ is $0$ and we get
$\phi(p) = p - 1$ and can say that $\mathbb Z^\times_p = \{1, 2, \ldots, p -
1\}$.

If $m$ and $n$ are two positive integers that have no factors other than $1$ in
common (another way of saying this is that $m, n$ are coprime) then the only
elements of $\mathbb Z_{m \times n}$ that have a factor in common are those that
already had a factor in common with $m$ or with $n$. In other words,

\begin{proposition}
Two integers $m, n$ are coprime if their greatest common divisor is $1$.
For coprime positive integers $m, n$ we have $\phi(m \times n) = \phi(m) \times
\phi(n)$.
\end{proposition}

In particular this holds when $m, n$ are distinct primes. For primes, we can
say even more: if we look at the numbers sharing a factor with $p^k$ for $p$ a
prime and $k$ a positive integer, these numbers must already share a factor
with $p$ since the factors of $p^k$ are exactly $1, p, p^2, \ldots, p^k$.
If we write the numbers from $1$ to $p^k$ in a $p$-column table, the table has
$p^k / p = p^{k-1}$ rows and each row contains exactly one multiple of $p$ in
the last column, so there are $(p-1)$ columns with numbers that are coprime to
$p$ per row. This lets us find $\phi(p^k)$:

\begin{proposition}
For a prime $p$ and a positive integer $k$ we have $\phi(p^k) = p^{k-1}(p-1)$.
\end{proposition}

The last two propositions allow us to find $\phi(n)$ for any $n$. We factor $n$
as $p_1^{a_1} \times \ldots p_k^{a_k}$ where the $p_1, \ldots, p_k$ are distinct
primes and $a_i$ is the number of times the prime $p_i$ appears. Then we have
\[
\phi(n) = \prod_{i=1}^k p_i^{a_i - 1} (p_i - 1)
\]

This concludes our little excursion to investigate Euler's $\phi$ function and
we return to showing that $(\mathbb Z^\times_n, \times_n)$, whose size we can
now compute, is a group.

\begin{exercise}
$(\star)$ \emph{Euler's $\phi$ function.}
How many elements are there in the group $(\mathbb Z^\times_n, \times)$ for

\begin{enumerate}
\item $n = 1009$ (this is a prime)
\item $n = 64$
\item $n = 60$
\end{enumerate}

\end{exercise}

\subsection{Exponent arithmetic}

We have learnt to add and multiply modulo $n$. Writing $[\ ]$ for reduction
modulo $n$, we have $[a + b] = [[a] + [b]]$ and $[a \times b] = [[a] \times
[b]]$. What about exponentiation?  Since it is usually defined by repeated
multiplication
\[
a^n := \underbrace{a \times a \times \ldots \times a}_{n \text{ times}}
\qquad (n \in \mathbb N)
\]
we obviously get $[a^n] = [[a]^n]$ and the usual rules such as $a^n \times b^n =
(a \times b)^n$ and $(a^n)^m = a^{n \times m}$ in any commutative ring.  Note
however that while $a, b$ can be elements of any commutative ring, $m, n$ are
integers --- exponentiation with exponents in any ring is not defined! Thus, in
the last formula, the multiplication in the exponent is normal integer
multiplication.  In a field, we can define exponentiation with negative
exponents the usual way $a^{-n} := (1/a)^n$ for any base except $0$ (we leave
$0^0$ undefined\footnotemark).
\footnotetext{But see also \texttt{https://tinyurl.com/zeropowerzero}
for a slightly humourous take on this.}

In some cases, we can reduce exponents as well to calculate more
efficiently. Let's try and calculate $3^{10} \pmod{5}$. Since we expect a
result in $\mathbb Z_5$, surely we can reduce that $10$? The wrong way to do
this is to note that $[10] = 0$ modulo $5$, so $3^0 = 1$. Wrong, because
$3^{10} = 59049 = 11809 \times 5 + 4$ so the correct answer is $4$, not $1$.
What went wrong?  The problem is that exponents, as we mentioned, are integers
and not ring elements --- even if ring elements are sometimes integers too. We
summarise:

\textbf{WARNING:} you can reduce $\pmod{n}$ at any time in addition,
subtraction, multiplication and (where defined) division in $\mathbb Z_n$. You
cannot reduce exponents this way.

There is a way to reduce the exponent correctly though. Recall that
$(Z^\times_n, \times)$ is a group, so each element $a$ of this group has an
order $k$ for which $a^k = 1$ in the group. And all these element orders must
divide the group order by Lagrange's theorem; the group order is of course
$\phi(n)$. So the correct way to reduce exponents is taking them modulo
$\phi(n)$ instead of $n$. In our example $3^{10} \pmod{5}$ we have $\phi(5) =
4$ so $3^{10} = 3^{10 \pmod{\phi(5)}} = 3^{10 \pmod{4}} = 3^2 = 4 \pmod{5}$.
So the correct formula is
\[
    a^k = (a \mod n)^{(k \mod \phi(n))} \pmod{n}
\]
using $[\ ]_n$ to denote reduction modulo $n$ we can also write this as
\[
[a^k]_n = [\ ([a]_n)^{[k]_{\phi(n)}}\ ]_n
\]
that is, you reduce group elements or bases modulo $n$ and exponents modulo
$\phi(n)$.
In the special case where $n$ is a prime, $\phi(n) = n - 1$.

For large values of $n$ and its prime factors, computing $\phi(n)$ is much more
time-consuming than simple computation modulo $n$: essentially, you have to
perform a task similar to factoring $n$ to get hold of $\phi(n)$. This forms
the basis of the famous RSA cryptosystem and much of modern cryptography that
has been developed since.

\begin{exercise}
$(\star\star)$ \emph{Exponentiation modulo $n$.}
Compute the following value:
\[
2^{128} - 1 \pmod{1009}
\]
Hint: you definitely do not want to compute all the digits of $2^{128}$!
You know that $[a + b] = [[a] + [b]]$ and $[a \times b] = [[a] \times [b]]$ where
$[\ ]$ is reduction modulo $1009$. You can also avoid doing $128$ individual
calculations and do $8$ instead.
\end{exercise}

\ifdefined\booklet
\else
\clearpage
\fi

\subsection{$\diamond$ Cancellation and integral domains}
\begin{diamondsec}
In a group, you can ``cancel'' in additions: $x + a = y + a$ implies $x = y$.
This holds because group elements have inverses: given $x + a = y + a$ you can
add the inverse of $a$ on both sides to get $(x + a) + (-a) = (y + a) + (-a)$,
swap the brackets round with the associative law to get $x + (a + (-a)) = y +
(a + (-a))$, use the property of inverses to get $x + 0 = y + 0$ and the
property of the neutral element to get $x = y$.

If an element in a ring is not zero or a zero-divisor, you can still cancel it
--- but the reason is a slightly different one. Suppose $a$ is such an element
and $a \times x = a \times y$. This implies that $a \times (x - y) = 0$ (subtract
$a \times y$ on both sides and use the distributive law) but since $a$ is not a
zero divisor, this means that $x - y = 0$ and therefore $x = y$.

A ring in which there are no zero divisors and you can cancel multiplication
with anything except $0$ is called an integral domain. The standard example of
an integral domain is $\mathbb Z$: $3 \times x = 3 \times y$ implies $x = y$
even without you having to extend to $\mathbb Q$ in order to invert $3$.

\begin{definition}[integral domain]
A ring $\mathcal R$ in which there are no zero divisors is called an integral
domain.
\end{definition}
\end{diamondsec}

\ifdefined\booklet
\else
\end{document}
\fi
