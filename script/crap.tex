\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{99}{Cryptographic Applications}

\begin{document}

\titlestuff
\fi

\section{Cryptographic Applications}

\emph{This appendix explains some of the applications of what we are learning in
CNuT to ancient and modern cryptography. The content of this section is not
required material for the CNuT assessment.}

\subsection{Groups and Subgroups}

The Enigma machine was an encryption device used by the German armed forces in
World War II. First the Polish and then the British forces managed to break the
Enigma cipher; for much of the war the British codebreaking team worked at
Bletchley Park, including Alan Turing who is often known as the ``Father of
Computer Science''.

The Enigma operated on the set $\{A, \ldots, Z\}$ of 26 letters. It contained
three cipher disks, each of which contained 26 pins on each side which were
connected by wires to the pins on the other side in order to produce a
permutation of the 26 letters. After each enciphered letter, the last wheel
advanced one position; after every 26 letters the second wheel advanced
(actually the position(s) when the wheels advanced was configurable, and formed
part of the key for each day).

Consider a single cipher wheel, implementing a permutation on the 26 letters.
Maybe it maps A to F, F to X and X to A. After the wheel rotates one position,
the permutation is a different one. But, the cycle structure of the permutation
stays the same: in the example above, one position further the wheel would map
B to G, G to Y and Y to B. If a wheel implements a permutation with cycles of
lengths e.g. 2, 7, 3, 5 and 9 then these cycle lengths will remain the same in
whichever position the wheel is rotated to.

Of course, the situation with three wheels is more complicated, and there was
also a plugboard and a reflector wheel. But exploiting the cycle lengths was
one of the many tricks used to break the Enigma; many of these tricks were
based on Group theory.

Even modern block ciphers are permutations. A block cipher, for a particular
key, takes a particular number of bits called the block size (e.g. 128 bits for
AES) and turns them into a different block of the same length. Because you need
to be able to decrypt again, this operation has to be invertible, so the block
cipher (for a fixed key) is a permutation on the set of possible blocks.

For different keys on the other hand, we want our ciphers \emph{not} to be a
group. Consider the monoalphabetic cipher, that is a permutation on the set of
26 letters. The key is the particular permutation. Can you make a
monoalphabetic cipher stronger by encrypting twice with different keys? No:
applying two permutations $p, q$ in a row is the same as applying the single
permutation $qp$ (or $pq$, depending on your notation). Encrypting twice with a
monoalphabetic is no better than encrypting once; the monoalphabetic cipher
forms a group.

When DES, the first modern commercial cipher, was published it had a key size
of 56 bits. If that is not enough for you, can you make DES stronger by
encrypting twice with different keys, giving you an effective key size of
112 bits? Put another way, according to the title of a 1988 paper by Ron Rivest
and other cryptographers, is DES a group\footnotemark?
\footnotetext{https://link.springer.com/article/10.1007/BF00206323}
The answer seems to be no, according to a 1992 paper\footnotemark.
\footnotetext{https://link.springer.com/chapter/10.1007/3-540-48071-4\_36}
In practice, if one still needs to use DES, one uses triple-DES rather than
double-DES, to avoid something called the ``meet in the middle'' attack.

\subsection{Rings and fields}

The RSA public-key encryption scheme uses a ring: to create a key pair, you
pick two primes $p, q$ as your secret key and set $N = pq$ as your public key,
together with a number $e$ that is coprime to $N$ as your encryption exponent.
To encrypt a message $m$, one computes $c = m^e \bmod N$. To decrypt, you find
the $d$ such that $ed = 1 \bmod \phi(N)$ and get $m = c^d \bmod N$ back.
security rests on the assumption that finding $d$ from $(e, N)$ requires working
modulo $\phi(N) = (p-1)(q-1)$ and computing this is as hard as factoring $N$.
The ring in question is $(\mathbb Z_N, +_N, \times_N)$ which has zero-divisors
(such as $p$ and $q$) but it is assumed that finding a zero-divisor is as hard
as factoring $N$.

The ``second generation'' of public-key cryptography operates over a finite 
field and particularly in vector spaces over a finite field. It lets you do 
ElGamal encryption, Schnorr zero-knowledge proofs and signatures, DSA (digital 
signature algorithm) and Diffie-Hellman key exchange (which was actually 
published before RSA). The fundamental operation here is scalar multiplication
because in suitable vector spaces, inverting this operation is assumed to be
hard.

The state of the art in public-key cryptography deployed in practice uses a
vector space defined as a set of points on an elliptic curve. For example, to
do Diffie-Hellman key exchange, there is a vector space $V$ over a field
$\mathbb F$ containing a publicly known base point $P \in V$. I pick a secret
$a \in \mathbb F$ at random and compute $A = a \times P$ and send $A$ to you;
you pick a secret $b$ and send $B = b \times P$ back to me. This gives us a
shared secret key that I can compute as $k = H(a \times B)$ where $H$ is some
hash function and you as $H(b \times A)$ since
$a \times B = (a \times b) \times P = (b \times a) \times P$.
Anyone intercepting our traffic just sees $(A, B)$ which is assumed not to be
sufficient to efficiently compute $k$.

The exact same operations can be done without elliptic curves, but at the cost 
of a larger bitlength of elements to get the same security level. Pick a prime 
$p$ and another prime $q$ that divides $p-1$. Pick an element $g$ that 
generates an order-$q$ subgroup of $Z^\times_p$. This group is your vector 
space, with $a \oplus b := (a \times_p b)$, e.g. the group operation is 
multiplication (not addition) modulo $p$, in a group of order $q$. All these 
parameters are public. This group is an $\mathbb F_q$ vector space with the 
scalar multiplication $a \otimes b = b^a \bmod p$ where $a$ is a scalar (in 
$\mathbb Z_q$) and $b$ is a ``vector'', e.g. an element of $\mathbb Z_p$ in the 
order-$q$ subgroup.

Diffie-Hellman key exchange works like this. I pick a secret $a \in \mathbb Z_q$
and send $A = g^a \bmod p$ (which is $a \otimes g$ in vector space notation).
You pick $b$ and send $B = g^b \bmod p$, e.g. $g$ is playing the same role that 
the point $P$ did in on the elliptic curve. The key is $k = H(A^b \bmod p)
= H(B^a \bmod p)$. Since we can write $k = H(K)$ with $K = g^{ab} \bmod p$,
one of the operations that has to be infeasible to compute is extracting an
exponent $z$ from a ``vector'' $g^z \bmod p$. Extracting an exponent like this
over the reals is called taking a logarithm, so this operation over a finite
field is called the \hi{discrete logarithm problem}. (The version over elliptic
curves is therefore called the elliptic curve discrete logarithm problem.)

\subsection{Polynomials and finite fields}

Polynomials appear in cryptography in Shamir's secret sharing (presented in the
last lecture), but also in the AES block cipher among other places.
AES is at heart a function $f: \{0,1\}^{128} \times \{0,1\}^k \to \{0, 1\}^{128}$ 
that encrypts 128-bit blocks to 128-bit blocks using a key that can be 128, 192
or 256 bits long. Within the AES function, certain operations are performed
repeatedly over multiple rounds. AES views an 128-bit block as a $4 \times 4$
matrix over the finite field $GF(2^8)$ using the irreducible polynomial
$X^8 + X^4 + X^3 + X + 1$.
One operation is called MixColumns and is simply multiplying the block with a
certain invertible matrix that has the effect of mixing up the data.
Another operation is called SubBytes and involves applying a function to
each element of $GF(2^8)$ in the block. The point of this function is to be
``as non-linear as possible''. The function is therefore based on the map
$x \mapsto 1/x$ in $GF(2^8)$ which, when written out in terms of coefficients
(viewing $GF(2^8)$ as an 8-dimensional vector space over $GF(2)$) is a
polynomial function.

\subsection{Linear algebra}

Linear algebra is an essential tool for working with finite fields; both elements
of AES in secret-key cryptography and zero-knowledge proofs in public-key
cryptography can be viewed as operations in vector spaces in finite fields and
this viewpoint is very useful for security proofs.

Another area in cryptography is lattice-based cryptography which has two
advantages, although it is not widely deployed as it is less efficient than 
finite-field cryptography. First, some lattice constructions are conjectured to
be secure even against a quantum computer which can run Shor's algorithm. This
algorithm is conjectured to solve both the factoring problem (breaking RSA)
and the discrete logarithm problem (breaking all the other public-key cryptography
that we use today). We will talk about the second one in the next section.

A lattice is more or less the set of points with integer coefficients in a
vector space over the real numbers. Which points end up in a lattice depends on
the choice of basis and one fundamental problem that is assumed to be hard on
large enough dimensions is turning a ``bad basis'' into a better one.

\subsection{Homomorphisms}

A public-key encryption scheme contains encryption and decryption algorithms
$E: PK \times M \to C$ and $D: SK \times C \to M$ where $M$ is the set of
messages, $C$ the set of ciphertexts and $PK, SK$ the set of public and secret
keys respectively.

Suppose that $M$ is a group with an operation $+: M \times M \to M$. A 
homomorphic encryption scheme is one with an operation $\oplus: C \times C \to 
C$ such that $E(pk, m_1) \oplus E(pk, m_2) \sim E(pk, m_1 + m_2)$, in other 
words the sum of two ciphertexts is a ciphertext for the sum of two messages.
For a fixed public key, this makes the encryption algorithm a group
homomorphism.
(The actual situation is a bit more complicated because ciphertexts can also
contain randomness.)

This lets you (almost) build a voting scheme: in a referendum, to vote ``no''
you submit an encryption of 0, to vote ``yes'' you submit an encryption of 1.
The election authorities add all the ciphertexts together and decrypt the result,
giving them the election result without ever learning how any individual voted.
(Again, the sitation in practice is a bit more complex as you have to prevent
someone from cheating by submitting an encryption of 2, but this involves adding
more components on to the basic idea sketched here.)

The second property of some lattice constructions is that they allow for a form 
of encryption known as \emph{fully homomorphic encryption} in which you can run 
arbitrary operations on encrypted data and get back an encryption of the 
result. Essentially, the encryption algorithm is a field homomorphism instead
of just a group homomorphism, and you can express any operation as an
\emph{arithmetic circuit} using just constants, addition and multiplication
just like you can express any operation on bits as a Boolean circuit using only
AND, OR and NOT gates.
(The actual running time of such schemes makes them of mostly theoretical 
interest at the moment, except for very particular kinds of operations.)

\subsection{Probability theory}

Probability theory is not only the basis for coding and information theory, but
also one of the pillars of cryptographic security proofs. Practical cryptographic
constructions usually do not have perfect security, so it is always possible in
theory to break them by guessing the secret key. A security proof therefore has
to show that the probability of finding any secrets using any combination of
random guessing and efficient computation is tiny, usually exponentially small
(for example $2^{-n}$ where $n$ is some ``security parameter'', roughly the same
concept in practice as the key length.)

\subsection{Coding theory}

The word ``code'' has multiple meanings. In this unit, we use code (encode, 
decode) to mean something that does not involve any secrets and cipher (encrypt,
decrypt) for something that does involve secrets. However, in the past the term
(secret) code was used for what we would call a cipher that operates at the word
instead of the symbol level --- countries used to equip their diplomats and
militaries with code books, a kind of dictionary in which common words and
phrases can be translated into codewords and back.

During the second world war, Bletchley Park build the first programmable
computer --- colossus --- to help solve messages encrypted with a special cipher
machine (the Lorentz machine) used by the Nazi high command. Colossus was not
used to break Enigma, as various badly informed articles sometimes state.
What we know of colossus is that it performed statistical analysis of the
received ciphertexts in order to narrow down the set of possible keys.

Early Cray supercomputers, modern Intel processors and graphics chipsets (e.g. 
NVIDIA/CUDA) have a POPCNT instruction\footnotemark{} which is often said to 
have been added at the direct request of the National Security Agency for 
cryptanalysis applications. One such source according to Wikipedia is the book 
``Hacker's delight'' by Warren, one of the better-known books on low-level 
implementation techniques. The POPCNT instruction simply computes the Hamming
weight of the value in a register, and it might well be exactly the kind of
instruction that needs to be really fast when you are doing statistical analysis
of ciphertext.
\footnotetext{See e.g. https://www.felixcloutier.com/x86/POPCNT.html}

\subsection{Information theory}

The entropy of a secret is an important measure in cryptography as a key with
$n$ bits of entropy should take an effort proportional to $2^n$ steps to break.
\hi{Key entropy} is what is really meant when one talks about \hi{key length},
as for example encoding a key in base64 to enable it to be stored and sent in
systems designed for textual data makes the key longer, but not any more secure.

When IBM designed what was later to become the Data Encryption Standard (DES),
they originally created a cipher with a 64-bit key length (nowadays, anything
below 128 is considered insecure and 256 bits is recommended for new systems).
The NSA reviewed their cipher and made two recommendations. One was to change
a table in a component called the S-box, another was to use one bit of each byte
as a checksum. The S-box change caused a lot of discussion as a lot of people
felt that the NSA had introduced a back door, especially as they refused to
explain the reason behind their changes in public. It later turned out that the
NSA's change to the S-box actually strengthened DES against an attack nowadays
known as \emph{differential cryptanalysis}, which was not known outside the NSA
(and perhaps to individuals in IBM) at the time. In the whole fuss about the
S-box, less publicity was given to the fact that the proposed checksum reduced
the entropy in a DES key to 56 bits, even though the key itself was still 64
bits long.

One of the consequences of Shannon's compression theorem is that \hi{uniformly
random data is not compressible}: a string of $n$ uniformly random bits has $n$
bits of entropy exactly. Therefore, any compression software that offers
something like ``guaranteed compression'' is making a false claim.

The Swiss cryptographer Maurer has even suggested using incompressibility as a 
measure of randomness: take any supposed random source, for example something 
that claims to be a cryptographic randomness generator, and run samples of its 
output through lots of known compression algorithms. If any of them finds a way 
to compress the samples that is on average better than doing nothing at all, 
then there is structure in the data and so it is not uniformly random. Of 
course, if this test fails to detect any structure then it does not mean your 
data really is uniformly random, but this test is good enough to catch out a lot
of claimed cryptographic random generators written by non-cryptographers.

Ciphertext in any reasonable encryption scheme should look uniformly random; in 
the symmetric cryptography world this is even one of the security notions 
(called IND-\$). In consequence, \emph{compression after encryption is 
stupid}. Compression before encryption is a good idea in theory, since it
reduces the redundancy in the messages which might help an attacker to break the
encryption. In practice however, a decent cipher nowadays should look uniformly
random whatever the message distribution, and compression-before-encryption was
one of the factors that was exploited in the CRIME and BREACH attacks against TLS.

\ifdefined\booklet
\else
\end{document}
\fi
