\section{Linear Codes}

A codeword is a sequence of symbols over some alphabet. If the alphabet happens
to be a finite field, we can view messages and codewords as vectors over that
field. It turns out that in this model, the most useful codes are linear
functions.

\begin{definition}[linear code]
Let $\mathbb F$ be a field, let $\mathcal M = \mathbb F^k$ and $\mathcal X =
\mathbb F^n$ which we view as vector spaces over $\mathbb F$.
Let $C \subseteq \mathcal X$ be a block code. The code is called
linear if it has the following properties:

\begin{itemize}
\item $C$ is a subspace of $\mathcal X$.
\item The encoding function $E: \mathcal M \to \mathcal X$ is linear.
\end{itemize}

We say that $C$ is a $[n, k]_q$ code if $C$ is a linear code, messages are
vectors of length $k$ and codewords have length $n$ and the field itself has
$q$ elements.

If we additionally know that $C$ has minimum distance $d$, we can call it a
$[n, k, d]_q$ code.
\end{definition}

If $C$ is a vector space, this means that the sum of any two codewords is again
a codeword. The dimension of $C$ as a vector space must be exactly $k$, which
means that $|C| = q^k$ since it must uniquely encode the $k$-dimensional message
space but a linear function cannot have a larger-dimensional image than its
domain.

\subsection{Generator matrices}

Of course, we can write linear functions as matrices. By convention in Coding
Theory, we multiply the message on the left rather than the right of the matrix
representing the encoding function, which we call a generator matrix:

\begin{definition}[generator matrix]
A generator matrix $G$ for a linear code $C$ as above is a $k \times n$ matrix
such that the encoding function is $E(m) = m \times G$ (where $m$ is represented
as a $1 \times k$ row vector).
\end{definition}

For example, over the field $\mathbb F_3$ (and in fact any other field) the
generator matrices for the 2-times repetitition code and the usual 1-symbol
checksum code on messages of length 3 are:
\[
    G_\text{rep} = \left(\begin{array}{llllll}
    1 & 0 & 0 & 1 & 0 & 0 \\
    0 & 1 & 0 & 0 & 1 & 0 \\
    0 & 0 & 1 & 0 & 0 & 1 \\
    \end{array}\right)
    \qquad
    G_\text{sum} = \left(\begin{array}{llll}
    1 & 0 & 0 & 1 \\
    0 & 1 & 0 & 1 \\
    0 & 0 & 1 & 1 \\
    \end{array}\right)
\]

What we called a \emph{checksum code} earlier has a more precise name. A code 
that turns $k$-symbol messages into $n$-symbol codewords (where $k \leq n$) 
such that a codeword is the message itself followed by $n-k$ further symbols 
(which one could view as a checksum) is called a \hi{systematic encoding} and 
in Linear Algebra terms, this means that the generator matrix starts with an 
identity matrix:

\begin{definition}[systematic encoding]
A systematic encoding is a $k \times n$ generator matrix (where $k \leq n$)
that starts with a $k \times k$ identity matrix and then has $n - k$ further
columns.
\end{definition}

You might wonder why one would ever use a non-systematic encoding. We will soon
see some ways of constructing codes that are not systematic, but the good news
is that any linear code can be turned into a systematic one with the same
parameters (in particular, minimum distance):

\begin{proposition}
If $C$ is a linear code with generator matrix $G$ then there is systematic 
encoding $G'$ of the same dimensions as $G$ that produces the same code $C$.
One can create $G'$ from $G$ by a combination of the following
linear operations on rows and columns of $G$:
\begin{itemize}
\item Multiply a row or column with a non-zero field element.
\item Add a multiple of one row to another.
\item Permuting rows or columns.
\end{itemize}
\end{proposition}

These are the operations you use to ``systematise'' a set of
linear equations, except that you can permute and scale colums as well
as rows (but you cannot add columns to each other).

This is another point where we need to be careful of the difference between a
code and an encoding. The systematized $G'$ may well produce a different
encoding to the original $G$, e.g. for a particular message $m$ we will
typically have $m \times G \neq m \times G'$ but the images of $G$ and $G'$ are
both the same vector space $C$.

This proposition is also useful when we are studying the efficiency of linear
codes. Instead of looking at all possible linear codes, we can consider only the
subset where the encoding is systematic: if there is an optimally efficient
linear code for certain parameters then there must be an equally efficient code
with a systematic encoding.

\subsection{Parity-check matrices}

On the decoding side, we have the following situation. Our code $C$ is a 
$k$-dimen\-sional subspace of the vector space $\mathcal X = \mathbb F^n$.
We would like to tell if a vector $x \in \mathcal X$ is in $C$, in which case
we assume that it was decoded correctly; otherwise we know there was at least
one error in transmission. (We will put off correcting errors until later.)

The way we achieve this is to project a received word $r$ onto the orthogonal
space $C^\bot$ of the code. If this gives the zero vector, we know $r \in C$.

\begin{definition}[parity-check matrix]
A parity-check matrix $H$ for a linear code $C \subseteq \mathcal X$ is a matrix
such that $c \in C \iff H c^T = 0$.

If $C$ has dimension $k$ and $\mathcal X$ has dimension $n$ then there exists a
parity-check matrix $H$ of dimension $(n - k) \times n$.
\end{definition}

For a systematic encoding, if we write the generator matrix as
$G = \left( \mathbb I_k \mid P \right)$
where $\mathbb I_k$ is a $k \times k$ identity matrix and $P$ is a $k \times (n-k)$
\hi{parity matrix} then one parity-check matrix is
$H = \left(-P^T \mid \mathbb I_{n-k} \right)$, that is the negative of the
transposed parity matrix followed by an identity matrix of size $n - k$.

\subsection{Distance of a linear code}

\begin{theorem}
The minimum distance of a linear code is equal to both of the following.
\begin{itemize}
\item The lowest Hamming weight of any non-zero codeword.
\item The minimal number of columns in the parity-check matrix needed to get
a set of linearly dependent columns.
\end{itemize}
\end{theorem}

We will prove the first point. For codewords which are vectors over a field, 
for any two codewords $c, c'$ we have $d(c, c') = d(c-c', c'-c') = d(c-c', 0)$ 
since adding or subtracting a constant to both elements does not add or remove 
differences. Since $c-c'$ is a codeword for any codewords $c, c'$ (as the
code is a subspace of a vector space), then for any pair $c, c'$ that attains
the minimal distance of the code we can find a codeword whose Hamming weight
is equal to the minimal distance. This shows that the minimal distance cannot be
larger than the lowest-weight codeword. Conversely, since the all-zero vector is
always a codeword in a linear code, $d(c, 0)$ cannot be smaller than the minimal
distance for any codeword $c$ either.

\subsection{The (7, 4) Hamming Code}

Once upon a time, Richard Hamming was using a computer operated by punched cards
that had a single parity bit per character. Reading in a program on a stack of
cards was a long operation and if the computer detected a single parity error,
it would stop and you had to start again from the beginning. Hamming wondered
whether the computer could not be modified to correct single-bit errors as well
as detecting them.

The (7, 4) code that Hamming invented in the 1950s encodes a 4 bits of data as
a 7-bit codeword; in our notation it is a code with $\Sigma = \mathbb F_2$,
$k = 4$ and $n = 7$. It is a linear code with the following three parity bits:
\[ \begin{array}{lllllllll}
p_1 & = & m_1 & + & m_2 & + & m_3 \\
p_2 & = & m_1 & + & m_2 & + & & & m_4 \\
p_3 & = & & & m_2 & + & m_3 & + & m_4
\end{array} \]

The following is a generator matrix in standard form for the (7, 4) Hamming code:
\[ G = \left( \begin{array}{lllllll}
1 & 0 & 0 & 0 & 1 & 1 & 0 \\
0 & 1 & 0 & 0 & 1 & 1 & 1 \\
0 & 0 & 1 & 0 & 1 & 0 & 1 \\
0 & 0 & 0 & 1 & 0 & 1 & 1 \\
\end{array} \right) \]

This code is linear, so we can compute the minumum distance by finding a 
lowest-weight nonzero codeword. A message with one or two bits set causes at 
least two parity bits to set, so all nonzero codewords have a weight of at 
least $d = 3$. This gives us single error correction or double error detection 
depending on whether we use strict or maximum-likelihood decoding.

The (7, 4) Hamming code is easiest to understand by looking at the following
diagram, for a word $r = m_1 m_2 m_3 m_4 p_1 p_2 p_3$:

\begin{center}
\begin{tikzpicture}

\draw (0, 0) circle (2cm);
\draw (2, 0) circle (2cm);
\draw (1, 1.75) circle (2cm);
\node at (-0.5, -0.5) { $p_2$ };
\node at (2.5, -0.5) { $p_3$ };
\node at (1, 2.5) { $p_1$ };
\node at (-0.25, 1.25) { $m_1$ };
\node at (1, 0.5) { $m_2$ };
\node at (2.25, 1.25) { $m_3$ };
\node at (1, -0.75) { $m_4$ };
\end{tikzpicture}
\end{center}

The idea is that the number of bits set in each circle must be even. If this is
the case then you have a codeword. If not, then assuming a single error the
error is in the area that represents the intersection of all the incorrect
circles and none of the correct ones.
Suppose you receive the word $r = 0100010$. The top circle has only 1 bit set, 
so something must have gone wrong. The left circle has 2 bits set, which is ok. 
The right circle has 1 bit set, which is also wrong. So the mistake must be in 
the intersection of the top and right circles, but not the left one, in other 
words $m_3$. The sent codeword (assuming a single error) must have been $c = 
0110010$, which we can decode as $m = 0110$.

\subsection{SECDED}

Although a code with distance $d = 3$ can correct a single error, with maximum 
likelihood decoding two errors are enough to produce an incorrectly decoded 
message. For this reason, although it is widely stated online that hardware (in 
particular, RAM) uses Hamming codes, what they actually use are Hamming codes 
with an extra parity bit that covers all message bits and the first three parity
bits. This gives a minimum  distance of 4 leading to both single error correction
and double error detection, also known as \hi{SECDED}.

To compute a generator matrix, note that 
$$p_4 = m_1 + m_2 + m_3 + m_4 + p_1 + p_2 + p_3$$
Substituting in the formulas for the other parity bits, this gives
$$p_4 = m_1 + m_3 + m_4$$
since anything added to itself cancels over $GF(2)$.
In terms of the generator matrix, this adds an extra column with a 0 in the second
position and 1s everywhere else.

Note that there can be different possible encodings for the same code.
For example, for the SECDED Hamming code an alternative is the generator
matrix consisting of a $4 \times 4$ identity matrix followed by the
``opposite'' matrix with 0s on the diagonal and 1s everywhere else.
