\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{21}{Cryptography}

\begin{document}

\titlestuff
\fi

\section{Cryptography}

In this lecture, we look at some cryptographic applications.

\subsection{The model}

For our first example, we consider a model in which Alice wants to send Bob a
message; for simplicity we assume the message is always an element of
$\mathcal M = \{0, 1\}^s$.
That is, Alice acts as a message source (with some probability distribution).

We have a channel from Alice to Bob, however this channel may be partly or
wholly controlled by an adversary Eve. Cryptographers distinguish the following
kinds of channels, among others:
\begin{itemize}
\item Insecure channel: when Alice sends a message, the message goes to Eve who
can choose what (if anything) to send on to Bob.
\item Authentic channel: when Alice sends a message, the message gets delivered
to Eve who can look at its contents and choose either to block it or to send it
on to Bob, but Eve cannot modify the message sent to Bob.
%\item Secret channel: when Alice sends a message, Eve gets notified but cannot
%see the message. Eve can choose to block the message, send it on unchanged, or
%replace it with a message of her own.
\item Secure channel: when Alice sends a message, Eve gets notified and can
choose whether the message should be sent to Bob or blocked. Eve cannot read or
modify the message.
\end{itemize}

Just like one can put encoders and decoders around a lossy channel, Alice and
Bob can put encryption and decryption around an authentic channel and hope that
this gives them the equivalent of a secure channel.

\begin{center}
\begin{tikzpicture}
    \node[draw] (alice) at (0, 0) {Alice};
    \node[circle,draw] (enc) at (2.5, 0) {Enc};
    \node[draw] (chan) at (5, 0) {channel};
    \node[circle,draw] (dec) at (7.5, 0) {Dec};
    \node[draw] (bob) at (10, 0) {Bob};
    
    \draw[-triangle 60] (alice) -- node[above] {M} (enc);
    \draw[-triangle 60] (enc) -- node[above] {C} (chan);
    \draw[-triangle 60] (chan) -- node[above] {C} (dec);
    \draw[-triangle 60] (dec) -- node[above] {M'} (bob);
    
    \node[draw] (eve) at (5, -1.5) {Eve};
    \draw[-triangle 60] (chan) -- node[right] {C} (eve);
\end{tikzpicture}
\end{center}

Suppose Alice sends a message $m \in \mathcal M$ picked according to a 
probability distribution $p$. Let $M$ be a random variable denoting the 
message. If Eve does not observe the message in transit, the message has $H(M)$ 
bits of entropy for Eve, that is if $V$ is a random variable for Eve's view
(which is ``nothing'' in this case) then $H(M | V) = H(M)$.

However, if Eve can make an observation, e.g. the first bit of the message or 
the whole message, then if we write $V$ as a random variable for Eve's 
observation then the remaining entropy is $H(M|V)$. Making an observation can 
only decrease the entropy in the message, never increase it.

\subsection{Perfect secrecy}

Suppose that Alice and Bob have agreed in advance on a shared secret key $k$ 
and they encrypt the message in transit. That is, There are encryption and 
decryption functions $E_k$ and $D_k$ such that $D_k(E_k(m))$. Alice computes $c 
= E_k(m)$ and sends this to Bob, who computes $m = d_k(c)$ to get the message 
back. In this case, even if the adversary Eve sees the whole transmission, they 
see $c$ and not $m$ itself. So let $C$ be a random variable for the encrypted 
message, a.k.a. ciphertext --- we will assume that the adversary can always 
observe the ciphertext. Ideally, the ciphertext reveals nothing about the 
message:

\begin{definition}
An encryption scheme $E_k, D_k$ is \hi{information-theoretically secret}
(sometimes also called \hi{perfect secrecy})
if for every distribution $M$ on messages and $C = E_k(M)$ we have
\[
    P(M = m \mid C = c) = P(M = m) \text{ for all } m, c
\]
\end{definition}

Written as entropies, we get the following lemma:

\begin{lemma}
In an encryption scheme with perfect secrecy,
\[
H(M \mid C) = H(M)
\]
\end{lemma}

To prove the lemma, we write out the formula for conditional entropy
where $\mathcal M, \mathcal C$ stand for the sets of possible messages and
ciphertexts:
\[
H(M \mid C) = - \sum_{c \in \mathcal C} p_C(c) \times \sum_{m \in \mathcal M} p_{M|C}(m, c) \log_2 p_{M|C}(m, c)
\]

Since $p_{M|C}(m, c)$ is just another way of writing $P(M = m \mid C = c)$,
we can replace this term by $p_M(m)$. The second sum does not contain any more
references to $c$ so we can pull it out of the first sum to get

\[
\left(- \sum_{m \in \mathcal M} p_M(m) \log_2(p_M(m))\right) \times
\left(\sum_{c \in \mathcal C} p_C(c)\right)
\]
The term on the left is the definition of $H(M)$ and the term on the right is
a sum over a probability distribution, so it has the value 1. This proves the
lemma.

Another equivalent way of formulating perfect secrecy is that there is zero 
mutual information between message and ciphertext, that is $I(M, C) = 0$.


\subsection{The one-time pad}

The \hi{one-time pad} is an information-theoretically secret cipher. It takes a 
key of $m$ bits (as long as the message) and can be used to send a single 
message (one can generate multiple keys to send multiple messages). It is also
called the \emph{Vernam cipher} after Gilbert S. Vernam (1890-1960) who patented
a version of it for teletypewriters at Bell labs.

The one-time pad may have been discovered as follows. Imagine a Caesar cipher
that shifts all letters by a uniformly random key value $k \in \mathbb Z_{26}$.
This is not secure at all, for one thing the key size is too small and for
another, letter patterns in words are not hidden. The next step up is to use
a fixed-length vector of key values. which gets you a Vigen\`ere cipher. This is
still not secure --- the usual way to break it is to find the length of the key
vector with statistical methods, then use frequency counts on all the letters
enciphered with the same key value. It is almost the logical next step to
imagine a key vector as long as the message itself (and used only for a single
message). This leaves absolutely no letter patterns in the message as every
ciphertext can stand for every possible message (of the same length).

The one-time pad can be defined over any finite group, although in practice one
mostly uses it over $GF(2^s)$, that is messages are bitstrings of length $s$
and the addition operation is bitwise XOR.

\begin{definition}[one-time pad]
The one-time pad for a finite group $G$ has keys, ciphertexts and messages all 
in $G$. To encrypt message $m$ with key $k$, set $c = m + k$. To decrypt 
ciphertext $c$ with key $k$, compute $m = c - k$.
\end{definition}

Let's prove that this is information-theoretically secret.

\begin{theorem}
The one-time pad is information-theoretically secret over any finite group $G$
if each key is chosen uniformly at random from $G$ and used only for a single
message.
\end{theorem}

To prove this, let $M$ be a random variable for a message, $K$ for a key and
$C$ for the ciphertext (message plus key). Further let $n = |G|$ be the group
size.

Start with the term $P(C = c \mid M = m)$, which is the
opposite way round (but we'll use Bayes' theorem on that later).
Since $c = m + k$, we can rewrite this as $P(K = (c-m) \mid M = m)$.
Using the definition of conditional probability, this expands to
$P(K = (c-m) \wedge M = m)/P(M=m)$. Since the key is independent of the message,
we can rewrite the numerator as $P(K=(c-m))P(M = m)$ and cancel the second
factor with the denominator leaving $P(K = (c-m))$. But the key is uniformly
random, so this term must be $1/n$.

Bayes' theorem gives us $P(M = m \mid C = c) = P(C = c \mid M = m) \times
P(M = m)/P(C = c)$
which becomes as $P(M = m) (1/n)/(1/n)$  and hence $P(M = m)$ since $C$ is 
uniform. This proves the theorem.

The one-time pad proof can also be done with the cryptographer's theorem.
Since the key $K$ is uniform and independent of the message $M$, the ciphertext
$C$ is therefore also uniform and independent of $M$. Independence is symmetric,
so $M$ is independent of $C$ and this immediately gives
$P(M = m \mid C = c) = P(M = m)$.

\subsection{Optimality of the one-time pad}

The one-time pad requires $m$ bits of key to encrypt $m$ bits of message.
Can we do better than this? Not if we want information-theoretic secrecy.

\begin{theorem}
Let $M, K, C$ be random variables for message, key and ciphertext in an
encryption scheme.
Any encryption scheme with information theoretic secrecy must have $H(K) \geq H(M)$.
\end{theorem}

This theorem was proven by Claude Shannon and is sometimes known as Shannon's
(perfect) secrecy theorem. We will not give a full proof here.

The intuition behind this theorem is as follows. Suppose our messages are
uniformly random in $\{0, 1\}^s$. Let's say $s = 8$ and you receive the ciphertext
$c = 01101111$. If the cipher is information-theoretically secret, then this
ciphertext must be able to stand for all $2^8$ possible messages, since if this
particular ciphertext could not stand for the message $m_0 = 00000000$ then just
seeing this ciphertext would let you deduce $m \neq m_0$, which gains you some
information on $m$. But if the ciphertext $c$ can stand for $2^8$ messages, then
there must be at least $2^8$ different possible keys.

In general, if we pick the uniform distribution $M$ on the message space 
$\mathcal M$ then this has an entropy of $H(M) = \log_2(|\mathcal M|)$ which is 
the highest possible entropy of any distribution on a set of size $|\mathcal M|$.
If the key space is the same size as the message space, it follows that the
distribution of keys must be uniform too as the uniform distribution is the
only distribution that attains the maximal possible entropy. We have just
established that the key size cannot be any smaller, but one could construct a
cipher where the key space is larger than the message space and have a non-
uniform distribution on the keys that still has enough entropy.

%\subsection{Authentication}

%Let's switch to a different model for a moment: Alice wants to send Bob a
%message $m$, but the message lands instead with Eve who can choose whether to
%send Bob the original message $m$ or some other message $m'$.

%Alice and Bob share a secret key $k$. Can they use this to ensure that Bob only
%accepts a message if Eve did not tamper with it? The cryptographic solution to
%this is called a MAC (message authentication code) and consists of a single
%function $M: \mathcal K \times \mathcal M \to \mathcal T$ where $\mathcal K$ is
%the set of keys, $\mathcal M$ is the set of messages and $\mathcal T$ is the
%set of (MAC) tags. Alice computes $t = M_k(m)$ and sends the pair $(m, t)$ to
%Bob. Bob, on receiving a pair $(m, u)$, accepts $m$ as authentic if and only if
%$u = M_k(m)$.

%\begin{theorem}
%There is no information-theoretically secure MAC scheme in this model, that is
%a function $M$ such that a perfect adversary Eve cannot forge a message (possibly
%with more than some tiny probability).
%\end{theorem}

%Suppose Alice wants to send $m$, so she sends $(m, t)$ with $t = M_k(m)$.
%If Eve sends $(m', t')$ to Bob with $t' = M_k(m')$ then Bob will accept this
%message as authentic. Whether Eve can find such a $t'$ or not depends on Eve's
%capabilities, but such a $t'$ must exist.

%In practice, we do have MAC functions where it is considered infeasible to find
%a tag $t'$ for any message $m'$ without being given the key. All this theorem
%says is that there cannot be information-theoretically secure MACs in this
%particular model.

\ifdefined\booklet
\else
\end{document}
\fi
