\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{4}{Fields, Euclid's algorithm}

\begin{document}

\titlestuff

\emph{In this lecture:} Fields --- Euclid's algorithm

\emph{Learning outcomes.}
After this lecture and revision, you should be able to:
\begin{itemize}
\item Apply Euclid's algorithm to integers $m, n$ to find $a, b$ with $a \times
n+b \times m = \gcd(m, n)$.
\item Explain what a field is.
\end{itemize}
\fi

\section{Fields and Euclid's algorithm}

\subsection{Fields}

The basic algebraic structures are the following:

\begin{itemize}
\item A monoid\footnotemark is a structure in which you can add.

\footnotetext{
We haven't formally covered monoids in this course but they are structures $(M,
+)$ where $+$ is associative and has a neutral element, but not necessarily
inverses.  If $(R, +, \times)$ is a ring then $(R, \times)$ is a monoid.
}

\item A group is a structure in which you can add and subtract.

\item A ring is a structure in which you can add, subtract and multiply.

\item A field is a structure in which you can add, subtract, multiply and
divide\footnotemark.

\footnotetext{Except that you cannot divide by zero.}
\end{itemize}

The mathematical definition of a field is a bit more formal than this:

\begin{definition}[field]
A structure $\mathbb F = (F, +, \times)$ is a field if it is a commutative
ring, $0 \neq 1$ and every element except $0$ is a unit.
\end{definition}

Another way of saying that $\mathbb F$ is a field is that $\mathbb F \setminus
\{0\}$ forms a (commutative) group under multiplication.

\subsection{Finite rings and fields}

The ring $(\mathbb Z, +, \times)$ has no zero-divisors, but does not have inverses
for all elements (in fact, only two elements are units). One can add the missing
inverses by introducing the rational numbers $\mathbb Q$ or ``fractions'', which
gives a field.

If we start with a finite ring $(\mathbb Z_n, +_n, \times_n)$, what happens?
To answer this question fully, we'll take a step back and look at rings in general.

Take any ring $(R, +, \times)$. We know that we can classify all elements as
zero, unit, zero divisor or neither. The ring is a field if all elements except
zero are units, or equivalently there are no zero divisors or ``neither'' elements.

But if our ring is finite, the ``neither'' case cannot happen:

\begin{proposition}
A finite commutative ring with $0 \neq 1$ and without zero divisors is a field.
\end{proposition}

Let's pick an element $a$ in a finite ring that is neither zero nor a zero
divisor. As long as $0 \neq 1$, there must be at least one such element.
This means that if we look at the sequence $a, a \times a, a \times a
\times a, \ldots$, we can never hit zero. We can obviously write this as $a,
a^2, a^3, \ldots$. But if the ring is finite, at some point an element has to
repeat so we get an equation of the form $a^k = a^{k + m}$ for positive
integers $k, m$ from which we conclude that $a^m = 1$ (you can cancel $a^k$
since $a$ is not a zero divisor, therefore neither is $a^k$).
Whether or not the ring is commutative, the associative law implies that powers
of $a$ commute with each other. Therefore $a \times a^{m-1} = 1$ and $a^{m-1}
\times a = 1$, so $a^{m-1}$ really is the inverse of $a$ under multiplication.

In other words, the only way a nonzero ring element can be neither a unit nor a
zero divisor is if the ring is infinite (for example, $3$ in $\mathbb Z$). In a
finite ring such as $\mathbb Z_n$, as soon as a nonzero element is not a zero
divisor it automatically has an inverse.

We know that the rings $(\mathbb Z_n, +_n, \times_n)$ satisfy these
conditions exactly when $n$ is a prime, except for the special case $(\{0\}, +,
\times)$.
(For a non-prime $n$, the structure $(\mathbb Z^\times_n, \times)$ is a group
but such a $\mathbb Z^\times_n$ is no longer a group under addition so we
cannot construct a ring this way, let alone a field.)
This means that for each prime $p$, we can construct a field with $p$ elements.

\subsection{Characteristics}

Can we construct any other kinds of finite fields apart from $\mathbb Z_p$ ?
We can, but it gets more complicated (and we'll do it in a later lecture).
Let's have a look at a problem we'll hit otherwise.

In a ring $(R, +, \times)$, consider the element $1_R$ (the neutral element of
multiplication) and the sequence $1_R, 1_R + 1_R, 1_R + 1_R + 1_R \ldots$.
One of two things can happen, and we give them a name:

\begin{definition}[Characteristic of a ring]
In a ring $(R, +, \times)$, if adding $1_R$ to itself never produces $0_R$
then we say that the ring has characteristic $0$.
Otherwise, if $n$ is the smallest number such that adding $1_R$ to itself $n$
times gives $0_R$, then we say the ring has characteristic $n$.
\end{definition}

Warning: it would be nice to write ``adding $1_R$ to itself $n$ times'' as
$n \times 1_R$. Unfortunately, this would mean defining a new kind of
multiplication that takes integers on the left and ring elements on the right,
and we'd have to check what other properties this operation has before we can
use them (is it still distributive, for example?). So we avoid such notation
for now.

For example, $\mathbb Z$ is a ring of characteristic $0$. In fact, a ring of
characteristic $0$ has to be infinite, since in a finite ring you eventually
hit a repeat element and thus we have to hit the neutral element (this is the
same reasoning as we used above for multiplication).

In our search for more finite fields, we observe:

\begin{lemma}[Characteristic of a field]
The characteristic of a field is either $0$ or a prime number.
\end{lemma}

As a consequence, the characteristic of a finite field must be a prime number.
The idea behind the proof is the following. In $\mathbb Z_6$, we have 
$1 + 1 + 1 + 1 + 1 + 1 = 0$, that's six ones and $6 = 2 \times 3$. So we can
group the ones (using associativity) as $(1 + 1 + 1) + (1 + 1 + 1) = 0$
and write this as $2 \times (1 + 1 + 1) = 0$ (which is of course just another
way of saying $2 \times 3 = 0$ modulo $6$). But this means that $2$ and $3$
are zero divisors, so $\mathbb Z_6$ cannot be a field.

The general version of this idea is as follows: let $R$ be a ring of finite
characteristic $n$ and suppose that $n$ is not a prime, that is $n = ab$ for
some integers $a, b > 1$. By definition of characteristic,
$1_R + \ldots + 1_R = 0_R$ where there are $n$ summands in the sum on the left.
Define $A$ to be $1_R + \ldots + 1_R$ where there are $a$ summands and define
$B$ to be the same sum with $b$ summands. Then we see that $A \neq 0_R$ as
$n$ was the smallest such sum that becomes $0_R$ again (by definition of
characteristic) and $a < n$, since $n = ab$ and $b > 1$. For the same reason,
$B \neq 0_R$. But $A \times_R B = 0_R$ since if you multiply out
$(1_R + \ldots + 1_R) \times (1_R + \ldots + 1_R)$ with $a$ resp. $b$ summands
then you get $ab = n$ summands. Therefore $A$ and $B$ are zero divisors.
In conclusion, if the characteristic of $R$ is neither $0$ nor prime then $R$
cannot be field.

\subsection{Towards classification of finite fields}

Which finite fields exist? We know that the characteristic of a finite field
must be a prime. Further we know that for every prime $p$ there is a field with
$p$ elements, namely $(\mathbb Z_p, +_p, \times_p)$.

The following is also a field however, showing that the number of elements in a
finite field does not have to be prime even if the characteristic is:

\[
\begin{array}{l|lllllllll}
+ & 0 & 1 & 2 & A & B & C & D & E & F \\
\hline
0 & 0 & 1 & 2 & A & B & C & D & E & F \\
1 & 1 & 2 & 0 & B & C & A & E & F & D \\
2 & 2 & 0 & 1 & C & A & B & F & D & E \\
A & A & B & C & D & E & F & 0 & 1 & 2 \\
B & B & C & A & E & F & D & 1 & 2 & 0 \\
C & C & A & B & F & D & E & 2 & 0 & 1 \\
D & D & E & F & 0 & 1 & 2 & A & B & C \\
E & E & F & D & 1 & 2 & 0 & B & C & A \\
F & F & D & E & 2 & 0 & 1 & C & A & B 
\end{array}
\quad
\begin{array}{l|lllllllll}
\times & 0 & 1 & 2 & A & B & C & D & E & F \\
\hline
0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
1 & 0 & 1 & 2 & A & B & C & D & E & F \\
2 & 0 & 2 & 1 & D & F & E & A & C & B \\
A & 0 & A & D & 2 & C & F & 1 & B & E \\
B & 0 & B & F & C & D & 1 & E & 2 & A \\
C & 0 & C & E & F & 1 & A & B & D & 2 \\
D & 0 & D & A & 1 & E & B & 2 & F & C \\
E & 0 & E & C & B & 2 & D & F & A & 1 \\
F & 0 & F & B & E & A & 2 & C & 1 & D
\end{array}
\]

There are some interesting facts about this finite field:

\begin{itemize}
\item The additive group is isomorphic to $\mathbb Z_3 \times \mathbb Z_3$,
e.g. each element can be seen as a pair of elements following the usual rules
for $\mathbb Z_3$. (The number $i$ maps to the pair $(i, 0)$ and $A = (0, 1)$.
\item The multiplicative group (with 0 removed) is actually $\mathbb Z_8$ with
1 the neutral element and B (for example) a generator.
\item The real trick is to see a pair $(a, b)$ as a polynomial $a + bX$ over
$\mathbb Z_3$. This explains addition. For multiplication, there is one single
rule that explains what to do when you get an $X^2$ term --- can you find it?
\end{itemize}

We will see later that ``up to isomorphism'' it is the only finite field with
9 elements.

\begin{exercise}
$(\star\star)$ \emph{A field with four elements.}

Find a field with four elements, that is write down the addition and
multiplication tables. We will learn an algorithm for doing this later on, but
for now note that there must be a neutral element for addition and one for
multiplication, call these 0 and 1. Call the other elements A and B. The
characteristic of the field gives you the layout of the additive group.
For the multiplication table, there is only one way to write it that respects
the commutative and distributive laws.
\end{exercise}

\subsection{Euclid's algorithm}

Our next step is a lemma by Euclid, a variation on division with remainder,
that comes with an algorithm to find the numbers in question:

\begin{lemma}[Euclidean algorithm]
If $m, n$ are two nonzero integers then there are unique integers $a, b$ such
that
\[
a \times n + b \times m = \gcd(m, n)
\]
\end{lemma}

This gives us the inverses we want in $\mathbb Z^\times_n$. If $m$ is coprime
to $n$ then the $\gcd$ is $1$ (or $m$ would not be in $\mathbb Z^\times_n$ at
all) and we find $a, b$ such that $a \times n + b \times m = 1$ which is
equivalent to saying that $b \times m = (-a) \times n + 1$ so $b \times m$ leaves
remainder $1$ when dividing by $n$ and $b \pmod{n}$ is the inverse we are
looking for. (We take $b$ modulo $n$ again because Euclid's algorithm can
return an $b$ outside the range $\mathbb Z_n$ in some cases.) This concludes
the proof that $(\mathbb Z^\times_n, \times_n)$ is a group.

Here's one way to compute the extended Euclidean algorithm.
Suppose we want to invert $17$ modulo $256$. That is, we want to find $b$
such that $17b = 1 \pmod{256}$, for which we find $a, b$ with Euclid's
algorithm such that $256a + 17b = 1$.
Make a four-column table with headings \textbf{q} (quotient), \textbf{r}
(remainder), \textbf{a} and \textbf{b}. Write the top two rows as in the table
below with the numbers in the \textbf{r} column and the rest as shown.

\begin{center}
\begin{tabular}{rrrr}
\textbf{q} & \textbf{r} & \textbf{a} & \textbf{b} \\
\midrule
  0 & 256 &   1 &   0 \\
  0 &  17 &   0 &   1 \\
 15 &   1 &   1 & -15 \\
 17 &   0 & -17 & 256
\end{tabular}
\end{center}

For all further rows, calculate as follows. Let $q', r', a', b'$ be the values
from the last row and $q'', r'', a'', b''$ be the second-to-last row (so for
the third row, $r' = 17$ and $r'' = 256$).
\begin{itemize}
\item Divide $r''$ by $r'$ with remainder. Put the quotient and remainder as
the new $q, r$ values.
\item Set $a := a'' - q \times a'$ and $b := b'' - q \times b'$.
\end{itemize}
For row $3$, we get $256 = 15 \times 17 + 1$ so we start the third row with $q =
15, r = 1$. Then $a := 1 - 0 \times 15 = 1$ and $b := 0 - 1 \times 15 = -15$.

Repeat until the remainder becomes $r = 0$. When this happens, the last row
with a nonzero remainder contains the important information: the $r$ value in
this row is the $\gcd$ of the two original numbers (in our case this is $1$,
without this we could not do modular inversion at all) and the $a, b$ values in
this row are the ones we are looking for. In our case $a = 1$ and $b = -15$ and
indeed, $1 \times 256 -15 \times 17 = 1$ so $-15 \mod{256} = 241$ is the
inverse of $17$ in $(\mathbb Z^\times_{256}, \times)$.

\subsection{Examination of Euclid's algorithm}

Why does this algorithm work? Let's look at a slightly simplified case where we
just want to solve an equation modulo $n$, which means finding a modular inverse.

Consider the equation $a X_1 = b \bmod{n}$ in one variable $X_1$ where $n$ is a
prime number (or at least $a \in \mathbb Z^\times_n$). We can rewrite this
equation as $a X_1 - b = 0 \bmod{n}$. This means that the integer $a X_1 - b$
must be a multiple of $n$, so we can introduce a new variable:
$a X_1 - b = X_2 n$ and rewrite the equation as $X_2 n + b = a X_1$. Since the
right-hand side is a multiple of $a$, so is the left so we can write
$X_2 n + b = 0 \bmod{a}$ which we rewrite as $n X_2 = (-b) \bmod{a}$.
We started with an equation with parameters $(a, b, n)$ and got a new one of the
same form with parameters $(n, -b, a)$. What does this buy us?

What it buys us is that in the original equation, since we are working modulo $n$
we can reduce the coefficients $a$ and $b$ modulo $n$ if this has not been done
already. In particular, $a < n$ as integers. In the second equation, since we
are working modulo $a$, not only has our modulus got smaller but we can reduce
$n$ and $b$ modulo $a$ too.

So we can write a table like this:

\begin{center}
\begin{tabular}{rrr}
\textbf{a}\hspace{1ex} & \textbf{b}\hspace{1ex} & \textbf{n} \\
\midrule
$[a]_n$ & $[b]_n$ & $n$ \\
$[n]_a$ & $[-b]_a$ & $a$ \\
\vdots & \vdots & \vdots 
\end{tabular}
\end{center}

The rule is, to create a new row put the value from the `a' column into the `n' 
column ($n = a'$), then reduce the previous `n' value modulo this value and put 
it into the `a' column ($a = [n']_{a'}$). In the `b' column, subtract the old
`b' value from the new `n' value and reduce if necessary ($b = [n - b']_n$).

The value in the `n' column gets smaller all the time but stays nonnegative, so 
this process will eventually stop --- in fact, it will terminate after at most
$O(\log_2(n))$ steps so it is an efficient algorithm when you count bit operations.

Let's try an example. Suppose $(a, b, n) = (91, 5, 251)$. The table becomes:

{
\renewcommand{\o}{\phantom{0}}
\begin{center}
\begin{tabular}{rrrll}
\textbf{a} & \textbf{b} & \textbf{n} & equation & relation \\
\midrule
91 &  5 & 251 & $ 91 X_1 = \o 5 \bmod 251$ \\
69 & 86 &  91 & $ 69 X_2 =   86 \bmod\o91$ & $91 X_1 - \o5 = 251 X_2$ \\
22 & 52 &  69 & $ 22 X_3 =   52 \bmod\o69$ & $69 X_2 - 86 = \o91 X_3$ \\
 3 & 14 &  22 & $\o3 X_4 =   14 \bmod\o22$ & $22 X_3 - 52 =  \o69 X_4$ \\
 1 &  1 &   3 & $\o1 X_5 = \o 1 \bmod\o\o3$ & $\o3 X_4 - 14 = \o22 X_5$
\end{tabular}
\end{center}
}

The last row represents the equation $1X_5 = 1 \bmod{3}$ which obviously has
the solution $X_5 = 1$. From this we can work backwards using the relations
above to get $X_4 = 12$, $X_3 = 40$, $X_2 = 54$ and $X_1 = 149$.
Indeed, $149 \times 91 = 54 \times 251 + 5$.

The extended Euclid's algorithm works on similar principles; the use of four
columns instead of three means you do not have to compute back up the whole
table again to find the $X_i$ values.

\subsection{Euclid's algorithm, recursive version}

The following algorithm computes Euclid's extended algorithm recursively.
It assumes a language with rounding-down integer division (indicated by
\texttt{//}) and that can return multiple values from a function; this could be
solved in other languages by returning a data structure.

\begin{verbatim}
-- returns values (x, y, g) such that
-- g = gcd(a, b) and ax + by = g
function xgcd(a, b)
    if b == 0 then
        return (1, 0, a)
    else
        (xx, yy, g) = xgcd(b, a % b)
        return (yy, xx - (a//b)*yy, g)
    end
end
\end{verbatim}

If $b \neq 0$ then suppose we have computed $(x', y', g') = \texttt{xgcd}(b, a 
\bmod b)$ and want to find $(x, y, g) = \texttt{xgcd}(a, b)$.

We know $g = g'$ since $\gcd(a,b) = \gcd(b, a \bmod b)$.

There are unique $(q, r)$ such that $a = qb + r$ with $0 \leq r < b$, using 
division with remainder. Since $ax + by = r$ then $q = a // b = (a-r)/b$ and $r 
= a \bmod b = a-qb$.

Substituting this into $bx' + (a \bmod b)y' = g'$ we get $b'x + (a-qb)y' = g$.
Rewrite this as $ay' + b(x'-qy') = g$ and subsitute back $q = a// b$ to get
$ay' + b (x' - (a//b)y') = g$, so $x = y'$ and $y = x' = (a//b) y'$ in the
equation $ax+by=g$. This gives the formulas for $x, y$ in the algorithm above.

\begin{exercise}
$(\star\star)$ \emph{Euclid's algorithm.}
\begin{enumerate}
\item Find integers $a, b$ such that $13 a + 64 b = 1$.
\item Find the inverse of $5$ under multiplication modulo $1009$.
\item Find all solutions of the equations $101 \times x = 1$ and $25 \times y + 6
= 98$ in $\mathbb F = (\mathbb Z_{1009}, +, \times)$.
\end{enumerate}
\end{exercise}

\begin{exercise}
$(\star\star)$ \emph{Classification of ring elements.}
Classify all elements of the rings $(\mathbb Z_n, +, \times)$ as zero, unit,
zero divisor or neither for the following values of $n$.

\begin{enumerate}
\item $n = 1009$ (this is still a prime)
\item $n = 64$
\item $n = 60$
\item Do the same for $(\mathbb Z, +, \times)$.
\end{enumerate}

\end{exercise}


\ifdefined\booklet
\else
\end{document}
\fi
