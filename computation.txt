Computation skills on CoCoNuT

Lecture 1
    addition modulo n
    division with remainder
Lecture 2
    permutations
    cycles
Lecture 3
    multiplication modulo n
    modular inverses and affine-linear equations mod n
    (all GF(p) arithmetic)
    Euler phi function
    Euclid xgcd algorithm
Lecture 4
    polynomial addition and multiplication (including mod n)
    polynomial division with remainder
Lecture 5
    Chinese remaindering
    mult. order of elements in rings
Lecture 6
    GF(p^n) arithmetic
    explicit representation of multiplication in GF(p)[X]/q(X)
    irreducible polynomials of low degree
    isomorphisms between representations of GF(p^n) in simple cases
Lecture 7
    matrix multiplication
    basis transformations
    automorphisms of [GF(p^n) : GF(p)] as linear transformations
